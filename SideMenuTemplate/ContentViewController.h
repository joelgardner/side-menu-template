//
//  ContentViewController.h
//  SideMenuTemplate
//
//  Created by Joel Gardner on 2/8/14.
//  Copyright (c) 2014 Joel Gardner. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RootViewControllerDelgate;

@interface ContentViewController : UIViewController
@property (nonatomic, weak) id<RootViewControllerDelgate> rootDelegate;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@end
