//
//  ContentViewController.m
//  SideMenuTemplate
//
//  Created by Joel Gardner on 2/8/14.
//  Copyright (c) 2014 Joel Gardner. All rights reserved.
//

#import "ContentViewController.h"
#import "RootViewController.h"

@interface ContentViewController ()

@end

@implementation ContentViewController
@synthesize rootDelegate, scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
       //self.navigationItem = [[UINavigationItem alloc] initWithTitle:@"asdf"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   //self.scrollView.contentSize = CGSizeMake(400, 1800);
   //self.scrollView.scrollEnabled = YES;
   self.scrollView.alwaysBounceVertical = YES;
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
   [super viewWillAppear:animated];
   //self.navigationController.navigationBar.barTintColor = [UIColor brownColor];
   //[self.navigationDelegate setTitle:@"Content View Title"];
   //NSLog(@"%f", self.scrollView.contentSize.height);
   //self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0 green:.8 blue:1 alpha:1];
   //[self.navigationController ad pushNavigationItem:[[UINavigationItem alloc] initWithTitle:@"Main Content"] animated:YES];
   //self.navigationController.navigationBar.translucent = YES;
   
   //CGSize s = self.scrollView.contentSize;
   //s.height += 1009;
   //self.scrollView.contentSize = s;
}
- (void)viewDidAppear:(BOOL)animated
{
   [super viewDidAppear:animated];
   
   self.title = @"Side Menu Template";
   //self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonItemStylePlain target:self action:@selector(derp:)];
}

//- (void)

@end
