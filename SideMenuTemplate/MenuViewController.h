//
//  MenuViewController.h
//  SideMenuTemplate
//
//  Created by Joel Gardner on 2/9/14.
//  Copyright (c) 2014 Joel Gardner. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RootViewControllerDelgate;

@interface MenuViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, weak) id<RootViewControllerDelgate> rootDelegate;
@property (nonatomic, strong) IBOutlet UITableView *tableView;

@end
