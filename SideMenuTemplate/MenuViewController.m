//
//  MenuViewController.m
//  SideMenuTemplate
//
//  Created by Joel Gardner on 2/9/14.
//  Copyright (c) 2014 Joel Gardner. All rights reserved.
//

#import "MenuViewController.h"
#import "RootViewController.h"

@interface MenuViewController ()
- (IBAction)closeMenu:(id)sender;
@end

@implementation MenuViewController
{
   NSArray *menuNodes;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
       menuNodes = @[@"Emergency", @"Maintenance", @"Maps"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    // Do any additional setup after loading the view from its nib.
   //[self.rootDelegate setPanContentView:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)closeMenu:(id)sender
{
   [self.rootDelegate closeMenu];
}

#pragma mark UITableViewDelegate & UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return [menuNodes count];
}

- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath
{
   return 6;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
   return 64.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
   UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, 22, 100, 40)];
   [titleLabel setText:@"Menu"];
   [titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
   
   UIView *header = [UIView new];
   [header addSubview:titleLabel];
   [header setBackgroundColor:[UIColor groupTableViewBackgroundColor]];

   return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   return 50.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   [self.rootDelegate didSelectMenuRow:indexPath.row];
   [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   static NSString *reuseIdentifier = @"MenuCell";
   
   
   UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
   if (!cell) {
      cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
   }
   
   cell.textLabel.text = [menuNodes objectAtIndex:indexPath.row];
   //cell.detailTextLabel.text = [menuNodes objectAtIndex:indexPath.row];
   return cell;
}

@end
