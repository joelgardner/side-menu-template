//
//  RootViewController.h
//  SideMenuTemplate
//
//  Created by Joel Gardner on 2/8/14.
//  Copyright (c) 2014 Joel Gardner. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RootViewControllerDelgate <NSObject>

- (void)closeMenu;
- (void)didSelectMenuRow:(NSInteger)row;

@optional
- (void)setTitle:(NSString *)title;

@end

@interface RootViewController : UIViewController<RootViewControllerDelgate>

@property BOOL panContentView;

@end
