//
//  RootViewController.m
//  SideMenuTemplate
//
//  Created by Joel Gardner on 2/8/14.
//  Copyright (c) 2014 Joel Gardner. All rights reserved.
//

#import "RootViewController.h"
#import "ContentViewController.h"
#import "MenuViewController.h"

const int kOpenXPosition = -70;
const int kDistanceToEdgeNeededToDragMenu = 70;
const BOOL shouldPanContentView = YES;

@interface RootViewController ()
{
   BOOL menuIsOpen;
}

@property (nonatomic, strong) IBOutlet UIView *contentView;
@property (nonatomic, strong) IBOutlet UIView *menuView;
@property (nonatomic, strong) UINavigationController *activeViewController;
@property (nonatomic, strong) MenuViewController *menuViewController;
@property (nonatomic, strong) ContentViewController *contentViewController;
@end

@implementation RootViewController
{
   UITapGestureRecognizer *tapRecognizer;
   UIPanGestureRecognizer *panRecognizer;
   CGFloat origX;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
       self.contentViewController = [[ContentViewController alloc] initWithNibName:@"ContentViewController" bundle:nil];
       self.contentViewController.rootDelegate = self;
       self.menuViewController = [[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil];
       self.menuViewController.rootDelegate = self;
       
       tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(respondToTapGesture:)];
       tapRecognizer.numberOfTapsRequired = 1;
       panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(respondToPanGesture:)];
       [self setGestureRecognizersEnabled:NO];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

   self.contentViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStylePlain target:self action:@selector(openMenu:)];
   self.activeViewController = [[UINavigationController alloc] initWithRootViewController:self.contentViewController];

   [self.contentView addSubview:self.activeViewController.view];
   [self.menuView addSubview:self.menuViewController.view];
   self.menuView.hidden = YES;

   [self.contentView addGestureRecognizer:tapRecognizer];
   [self.view addGestureRecognizer:panRecognizer];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
   self.menuView.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
   [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
   [super didReceiveMemoryWarning];
}

- (void)setTitle:(NSString *)title
{

}

#pragma mark Tap Gesture Recognizer 
- (void)respondToTapGesture:(id)sender
{
   if (menuIsOpen) {
      [self closeMenu];
   }
}

- (void)respondToPanGesture:(UIPanGestureRecognizer *)_panRecognizer
{
   CGPoint location = [panRecognizer locationInView:self.menuView];
   
   // distance from the edge of the menu to the user's finger
   int distanceToMenuEdge = location.x - self.menuView.frame.size.width;
   
   if (panRecognizer.state == UIGestureRecognizerStateBegan) {
      origX = self.menuView.frame.origin.x;
      if (distanceToMenuEdge < kDistanceToEdgeNeededToDragMenu) {
         self.menuView.hidden = NO;
      }
   }
   else if (panRecognizer.state == UIGestureRecognizerStateChanged && distanceToMenuEdge < kDistanceToEdgeNeededToDragMenu) {
      CGPoint t = [panRecognizer translationInView:self.menuView];
      CGFloat newX = origX + t.x;
      if (newX > kOpenXPosition)  {
         newX = kOpenXPosition;
      }
      else if (newX < -self.menuView.frame.size.width) {
         newX = -self.menuView.frame.size.width;
      }
      self.menuView.frame = CGRectMake(newX, 0, self.menuView.frame.size.width, self.menuView.frame.size.height);
      if (shouldPanContentView) {
         self.contentView.frame = CGRectMake(newX + self.menuView.frame.size.width, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
      }
   }
   else if (panRecognizer.state == UIGestureRecognizerStateEnded) {
      CGPoint v = [panRecognizer velocityInView:self.view];
      CGFloat duration = (duration = 300.0 / abs(v.x)) > 0.22 ? 0.22 : duration;
      
      // if any part of menu is visible, open/close it according to the drag's velocity (left or right)
      if (self.menuView.frame.origin.x != -self.menuView.frame.size.width || distanceToMenuEdge < kDistanceToEdgeNeededToDragMenu) {
         [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.menuView.frame = CGRectMake(v.x < 0 ? -self.menuView.frame.size.width : kOpenXPosition, 0, self.menuView.frame.size.width, self.menuView.frame.size.height);
            if (shouldPanContentView) {
               self.contentView.frame = CGRectMake((v.x < 0 ? -self.menuView.frame.size.width : kOpenXPosition)+self.menuView.frame.size.width, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
            }
         } completion:^(BOOL finished) {
            if (finished) {
               [self setGestureRecognizersEnabled:v.x >= 0];
               menuIsOpen = v.x >= 0;
               if (!menuIsOpen) {
                  self.menuView.hidden = YES;
               }
            }
         }];
      }
   }
}

- (void)openMenu:(id)sender
{
   menuIsOpen = YES;
   self.menuView.hidden = NO;
   [UIView animateWithDuration:0.22 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
      self.menuView.frame = CGRectMake(kOpenXPosition, 0, self.menuView.frame.size.width, self.menuView.frame.size.height);
      if (shouldPanContentView) {
         self.contentView.frame = CGRectMake(kOpenXPosition + self.menuView.frame.size.width, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
      }
   } completion:^(BOOL finished) {
      if (finished) {
         [self setGestureRecognizersEnabled:YES];
      }
   }];
}

- (void)closeMenu
{
   menuIsOpen = NO;
   [UIView animateWithDuration:0.22 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
      self.menuView.frame = CGRectMake(-self.menuView.frame.size.width, 0, self.menuView.frame.size.width, self.menuView.frame.size.height);
      if (shouldPanContentView) {
         self.contentView.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
      }
   } completion:^(BOOL finished) {
      if (finished) {
         [self setGestureRecognizersEnabled:YES];
         self.menuView.hidden = YES;
      }
   }];
}

- (void)setGestureRecognizersEnabled:(BOOL)enabled
{
   tapRecognizer.enabled = enabled;
   panRecognizer.enabled = YES;
}

- (void)didSelectMenuRow:(NSInteger)row
{
   [self performSelector:@selector(closeMenu) withObject:nil afterDelay:0.2f];
   NSLog(@"user selected row %d", row);
}

@end
